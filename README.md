# README

My event scheduling app consists of three controllers. The static_pages controller manages all the routing to the static pages of the website including About, Help, and the home page. The event controller manages the addition and deletion of events from the event model. The event controller is used for routing when an event is created or deleted. The attendee controller manages the addition, deletion, and check-in status of attendess for each event and is used for routing whenever a new attendee checks in. As soon as an event is deleted, all the attendees that were checked-in to that event are also wiped from the database.

As mentioned above, I have two database models. The event model contains all the events and their start times, while the attendee model contains all check-in attendees, their names, emails, and which event they check into. Using the event parameter of the attendee, I can effectively link the attendee model to the event model.


* ...
