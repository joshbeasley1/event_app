Rails.application.routes.draw do
  get 'attendees/new'

  get 'events/new'

  #root 'static_pages#home'
  root 'events#index'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'events#new'
  post '/signup',  to: 'events#create'
  get '/checkin', to: 'attendees#new'
  post '/checkin',  to: 'attendees#create'
  delete '/logout',  to: 'events#destroy'

  resources :events
  resources :attendees
end