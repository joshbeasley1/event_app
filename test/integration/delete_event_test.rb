require 'test_helper'

class DeleteEventTest < ActionDispatch::IntegrationTest
  def setup
    @event = events(:one)
  end

  test "successful event deletion" do
    get root_path
    assert_template 'events/index'
    assert_difference 'Event.count', -1 do
      delete event_path(@event)
    end
  end
end
