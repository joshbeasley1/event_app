require 'test_helper'

class DeleteAttendeeTest < ActionDispatch::IntegrationTest
  def setup
    @event = events(:one)
  end

  test "successful Attendee deletion" do
    get root_path
    assert_template 'events/index'
    assert_difference Attendee.count.to_s, 0 do
    	delete event_path(@event)
    end
  end
end
