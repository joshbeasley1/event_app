require 'test_helper'

class CreateEventTest < ActionDispatch::IntegrationTest

  def setup
    @event = Event.new(name: "Example User", starts_at: "2018-12-27 12:52:41")
  end

  test "invalid event creation information" do
    get signup_path
    assert_no_difference 'Event.count' do
      post events_path, params: { event: { name:  "",
                                         starts_at: ""} }
    end
    assert_template 'events/new'
  end

  test "valid event creation information" do
    get signup_path
    assert_difference 'Event.count', 1 do
      post events_path, params: { event: { name:  @event.name,
                                         starts_at: @event.starts_at} }
    end
    follow_redirect!
    assert_template 'events/index'
  end
end
