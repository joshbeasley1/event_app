require 'test_helper'

class AddAttendeeTest < ActionDispatch::IntegrationTest

  def setup
		@event = events(:one)
		@attendee = attendees(:josh)
	end

 test "correct starting number of attendees" do
    get signup_path
    assert_no_difference 'Attendee.count' do
      post events_path, params: { event: { name:  @event.name,
                                         starts_at: @event.starts_at} }
    end
    follow_redirect!
    assert_template 'events/index'
  end

 test "invalid attendee" do
		get checkin_path
		assert_no_difference 'Attendee.count' do
			post attendees_path, params: { attendee: { name: "", email: "" , event_id: @event.id} } 
		end
	end


 test "valid attendee difference test" do
    get checkin_path
    assert_difference 'Attendee.count', 0 do
      post attendees_path, params: { attendee: { name:  @attendee.name,
                                         email: @attendee.email,
                                         event_id: @event.id} }
    end
  end

end
