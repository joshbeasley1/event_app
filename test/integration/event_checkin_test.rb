require 'test_helper'

class EventCheckinTest < ActionDispatch::IntegrationTest
  test "invalid checkin information" do
    get checkin_path
    assert_no_difference 'Attendee.count' do
      post attendees_path, params: { attendee: { name:  "",
                                         email: "user@invalid",
                                         event_id:  0} }
    end
    assert_template 'attendees/new'
  end
end
