require 'test_helper'

class AttendeesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get attendees_new_url
    assert_response :success
  end

  test "should get checkin" do
    get checkin_path
    assert_response :success
    assert_select "title", "Check-In | Event Scheduling App"
  end

end
