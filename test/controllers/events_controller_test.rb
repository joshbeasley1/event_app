require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get events_new_url
    assert_response :success
  end

  test "should get create event" do
    get signup_path
    assert_response :success
    assert_select "title", "Create Event | Event Scheduling App"
  end
end
