require 'test_helper'

class AttendeeTest < ActiveSupport::TestCase
  def setup
    @event = Event.new(name: "Example User", starts_at: "2018-12-27 12:52:41")
    @attendee = @event.attendees.build(name: "Example User", email: "user@example.com")
  end

  test "should be valid" do
    assert @attendee.valid?
  end

  test "name should be present" do
    @attendee.name = ""
    assert_not @attendee.valid?
  end

  test "email should be present" do
    @attendee.email = "     "
    assert_not @attendee.valid?
  end

  test "name should not be too long" do
    @attendee.name = "a" * 51
    assert_not @attendee.valid?
  end

  test "email should not be too long" do
    @attendee.email = "a" * 244 + "@example.com"
    assert_not @attendee.valid?
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @attendee.email = invalid_address
      assert_not @attendee.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @attendee.dup
    duplicate_user.email = @attendee.email.upcase
    @attendee.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @attendee.email = mixed_case_email
    @attendee.save
    assert_equal mixed_case_email.downcase, @attendee.reload.email
  end
end
