require 'test_helper'

class EventTest < ActiveSupport::TestCase

  def setup
    @event = Event.new(name: "Example User", starts_at: "2018-12-27 12:52:41")
  end

  test "should be valid" do
    assert @event.valid?
  end

  test "name should be present" do
    @event.name = "     "
    assert_not @event.valid?
  end

  test "time should be present" do
    @event.starts_at = "     "
    assert_not @event.valid?
  end

  test "name should not be too long" do
    @event.name = "a" * 51
    assert_not @event.valid?
  end

  test "time should not be too long" do
    @event.starts_at = "a" * 51
    assert_not @event.valid?
  end
end
