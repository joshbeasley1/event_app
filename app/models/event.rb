class Event < ApplicationRecord
	has_many :attendees, dependent: :destroy
	validates :name, presence: true, length: { maximum: 50 }
	validates :starts_at, presence: true, length: { maximum: 50 }
end
