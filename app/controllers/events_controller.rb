class EventsController < ApplicationController
  def show
    @event = Event.find(params[:id])
  end

  def index
  	@events = Event.all
  	@attendees = Attendee.all
  end

  def new
  	@event = Event.new
  end

  def create
  	 @event = Event.new(event_params)
    if @event.save
      flash[:success] = "Thank you for registering your event"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def destroy
  	e = Event.find(params[:id])
  	# no longer need this, taken care of through has_many association
  	#Attendee.all.each do |a|
  	#  if a.event_name == e.name
  	#  	a.destroy
  	#  end 
  	#end
  	e.destroy
    flash[:success] = "Event deleted"
    redirect_to root_url
  end

  def event_params
      params.require(:event).permit(:name, :starts_at)
    end
end
