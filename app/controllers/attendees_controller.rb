class AttendeesController < ApplicationController
  def new
  	@attendee = Attendee.new
  end

  def create
  	 @attendee = Attendee.new(attendee_params)

    if @attendee.save
      flash[:success] = "Thank you for checking-in"
      redirect_to root_path
    else
     render 'new'
    end
  end

  def attendee_params
      params.require(:attendee).permit(:name, :email, :event_id)
  end
end
