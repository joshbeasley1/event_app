FactoryGirl.define do

  factory :event do
    name "Event 1"
    starts_at "2019-01-30 12:37:29"
  end

end