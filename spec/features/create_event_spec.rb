require "rails_helper"

RSpec.feature "Event created" do
	scenario "successfully" do

		visit root_path
		click_on "Create Event"
		fill_in "Name", with: "Event"
		click_on "Create Event"

		expect(page).to have_css '.event'
	end
end