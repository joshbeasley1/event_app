require "rails_helper"

RSpec.feature "Attendee added" do
	scenario "successfully" do

		visit root_path
		click_on "Create Event"
		fill_in "Name", with: "Event"
		click_on "Create Event"
		click_on "Check-In"
		fill_in "Name", with: "Josh"
		fill_in "Email", with: "josh@example.com"
		click_on "Check-In"

		expect(page).to have_css '.attendee'
	end
end