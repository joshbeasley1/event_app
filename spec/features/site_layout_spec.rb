require "rails_helper"

feature "Visit homepage" do 
	scenario "successfully" do
		visit root_path

		expect(page).to have_css 'h1', text: 'Welcome to the Event Scheduling App'
	
		visit about_path

		expect(page).to have_css 'title', text: 'About | Ruby on Rails Tutorial Sample App'

		click_on "Home"

		expect(page).to have_css 'h1', text: 'Welcome to the Event Scheduling App'

	end
end