require "rails_helper"

RSpec.feature "Attendee deleted" do
	scenario "successfully" do

		visit root_path
		
		click_on "Delete Event"

		expect(page).not_to have_css '.attendee'
	end
end