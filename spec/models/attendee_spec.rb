RSpec.describe Attendee, :type => :model do
  subject { Event.new(name: "Example User", starts_at: "2018-12-27 12:52:41").attendees.build(name: "Example User", email: "user@example.com") }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without an email" do
      subject.email = nil
      expect(subject).to_not be_valid
    end

    it "name should not be too long" do
      subject.name = "a" * 51
      expect(subject).to_not be_valid
    end

    it "email should not be too long" do
      subject.email = "a" * 244
      expect(subject).to_not be_valid
    end

    it "email validation should reject invalid addresses" do
      invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
      invalid_addresses.each do |invalid_address|
        subject.email = invalid_address
        expect(subject).to_not be_valid
      end
    end
  end
end