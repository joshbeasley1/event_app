RSpec.describe Event, :type => :model do
  subject { Event.new(name: "Event 1", starts_at: "2019-01-30 12:50:01") }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a start time" do
      subject.starts_at = nil
      expect(subject).to_not be_valid
    end

    it "name should not be too long" do
      subject.name = "a" * 51
      expect(subject).to_not be_valid
    end

    it "starts_at should not be too long" do
      subject.starts_at = "a" * 51
      expect(subject).to_not be_valid
    end
  end
end