class CreateAttendees < ActiveRecord::Migration[5.1]
  def change
    create_table :attendees do |t|
      t.string :name
      t.string :email
      t.belongs_to :event, index: true

      t.timestamps
    end
  end
end
